package com.example.rssiscanner;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


/**
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
public class DeviceAdapter extends ArrayAdapter<Device> {

	public DeviceAdapter(Context context, ArrayList<Device> devices) {
		super(context, 0,devices);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		Device device = getItem(position);
		
		if(convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_device, parent, false);
		}
		
		TextView deviceName = (TextView) convertView.findViewById(R.id.device_name);
		TextView rssi = (TextView) convertView.findViewById(R.id.device_rssi);
		TextView average = (TextView) convertView.findViewById(R.id.average_discovery_time);
		
		deviceName.setText(device.DeviceName + " (" + device.MACAddress + ")");
		rssi.setText("RSSI: " + Integer.toString(device.RSSI));
		average.setText("Average Milis: " + Float.toString(device.average));
		return convertView;
	}
	
}
