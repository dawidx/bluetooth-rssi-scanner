package com.example.rssiscanner;

import android.util.Log;

/**
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
public class Device {
	
	public String DeviceName;
	public String MACAddress;
	public int RSSI;
	public long lastTime;
	
	public float average = 0;
	
	public Device(String deviceName, String MACAddress, int rssi, long currentTime) {
		this.DeviceName = deviceName;
		this.MACAddress = MACAddress;
		this.RSSI = rssi;
			this.lastTime = currentTime;
	}
	
	public void updateAverage(long currentTime) {
		long interval = currentTime - lastTime;
		Log.d("Device", "Interval: " + interval + "");
		this.average = (this.average+(float)interval)/2f;
		Log.d("Device", "Average:" + this.average);
		this.lastTime = currentTime;
	}
	
	@Override
	public boolean equals(Object d) {
		if(d != null && d instanceof Device) {
			if(((Device) d).MACAddress.equals(this.MACAddress)) {
			return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
}
