package com.example.rssiscanner;

import java.util.ArrayList;

import android.support.v7.app.ActionBarActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.ToggleButton;

/**
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
public class MainActivity extends ActionBarActivity {

	ArrayList<Device> devices = new ArrayList<Device>();
	ListView devicesList;
	DeviceAdapter adapter;
	IntentFilter intentFilter;
	BluetoothAdapter bluetoothAdapter;
	BroadcastReceiver receiver = new BroadcastReceiver() {

		public static final String TAG = "Bluetooth Broadcast Receiver";

		@Override
		public void onReceive(Context context, Intent intent) {
			try {
			Log.d(TAG, "onReceive()");
			String action = intent.getAction();
			if(BluetoothDevice.ACTION_FOUND.equals(action)) {
				this.onDeviceFound(intent);
			} else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
				this.onDiscoveryFinished();
			} else {
				Log.w(TAG, "An event is attached to a broadcast receiver which is not handled");
			}
			}catch(Exception e) {
				Log.w(TAG, e.getMessage());
			}
		}

		private void onDeviceFound(Intent intent) {
			try {
				BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				if(!device.getName().equals("null")) {
					Log.i(TAG, "Device found");
					Device d = new Device(device.getName(), device.getAddress(), Integer.valueOf((int)intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,Short.MIN_VALUE)), System.currentTimeMillis());
					if(devices.contains(d)) {
						Device s = devices.get(devices.indexOf(d));
						s.RSSI = Integer.valueOf((int)intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,Short.MIN_VALUE));
						s.updateAverage(System.currentTimeMillis());
						adapter.notifyDataSetChanged();
					} else {
						devices.add(d);
						adapter.notifyDataSetChanged();
					}

				}
			} catch(Exception e) {
				Log.w(TAG, e.getMessage());
			}
		}

		private void onDiscoveryFinished() {
			Log.d(TAG, "Discovery finished. Repeating...");
			bluetoothAdapter.startDiscovery();
		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		adapter = new DeviceAdapter(this, devices);

		devicesList = (ListView) findViewById(R.id.expandableListView1);
		devicesList.setAdapter(adapter);

		intentFilter = new IntentFilter();
		intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
		intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		ToggleButton b = (ToggleButton) findViewById(R.id.start_stop_button);
		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				// Is the toggle on?
				boolean on = ((ToggleButton) view).isChecked();

				if (on) {
					startDiscovery();
				} else {
					stopDiscovery();
				}
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	public void startDiscovery() {
		registerReceiver(receiver, intentFilter);

		bluetoothAdapter.startDiscovery();
	}
	
	@Override
	public void onPause() {
		this.stopDiscovery();
		super.onPause();
	}

	public void stopDiscovery() {

		bluetoothAdapter.cancelDiscovery();
		unregisterReceiver(receiver);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
